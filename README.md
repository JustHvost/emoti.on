emoti.on
========

“We transform your love in sentences.”

Give it a try: http://www.justhvost.org/emoti.on


emoti.on 0.2b
-----------
[+] Now the “submit” button won't show anymore when you click it.

[+] Added a check for empty strings on custom mode.

[+] Now the logo in api.html has the cute heart too.


emoti.on 0.2a [aka “Coffee Edition”]
-----------

[+] New “submit” button

[+] New sentences


[+] Added the custom mode

[+] Added the custom mode (powered by coffee... SO MUCH COFFEE)


[+] Added “custom” button

[+] New background in api.html

[+] New animations

[+] New alphabet for encryption

[+] New font and color in api.html

[+] Replaced “+“ to “\” for the split between the two names


emoti.on 0.1b
-----------

[+] Some CSS fixes for other browsers

[+] Added a link to “index.html” in api.html

[+] Added a cute heart on the main logo


emoti.on 0.1a
-----------

[+] First release
