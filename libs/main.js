/*
 *	The MIT License (MIT)
 *
 *	Copyright (c) 2013 Giuseppe “JustHvost” D`Amico
 *
 *	Permission is hereby granted, free of charge, to any person obtaining a copy
 *	of this software and associated documentation files (the "Software"), to deal
 *	in the Software without restriction, including without limitation the rights
 *	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *	copies of the Software, and to permit persons to whom the Software is
 *	furnished to do so, subject to the following conditions:
 *
 *	The above copyright notice and this permission notice shall be included in
 *	all copies or substantial portions of the Software.
 *
 *	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *	THE SOFTWARE.
 */

function iswhite(string) { 
	var regexp = /^\s+$/; 
	if (regexp.test(string))
		return true;
	else
		return false;
}

function names(string, action) {
	var classic  = "abcdefghijklmnopqrstuvwxyz.,:;? àòèéùì!€$%()ABCDEFGHIJKLMNOPQRSTUVWXYZÈÉ";
	var not_classic  = "ypltavkrezgmshubxncdijfqow12345%[]{}#@^<>?,YPLTAVKREZGMSHUBXNCDIJFQOW;|";
	var newname = '';
	if (action == 'crypt') {
		for(var i=0;i<string.length;++i) {
			charPos = classic.indexOf(string.charAt(i));
			newname += not_classic.charAt(charPos);
		}
	} else if (action == 'decrypt') {
			for(var i=0;i<string.length;++i) {
				charPos = not_classic.indexOf(string.charAt(i));
				newname += classic.charAt(charPos);
		}
	}
	return newname;
}

function get_love(string){
	if (string == "default") {
		var name = document.getElementById("name").value;
		var name2 = document.getElementById("name2").value;
		if(!iswhite(name) && !iswhite(name2) && name != '' && name2 != ''){
			var name_low = name.toLowerCase();
			var name2_low = name2.toLowerCase();
			var him = names(name_low, 'crypt');
			var her = names(name2_low, 'crypt');
			$('input#link').val('www.justhvost.org/emoti.on/api.html?service='+him+'\\'+her+'&custom=false');
			got_love_here('1');
		} else
			alert("Error: No white space or empty strings\n\nTry again, please :)");
	} else if (string == "custom") {
		var name = document.getElementById("name3").value;
		var custom = document.getElementById("content").value;
		if(!iswhite(name) && name != '' && custom != ''){
			var name_low = name.toLowerCase();
			var name_crypt = names(name_low, 'crypt');
			var sentences_crypt = names(custom, 'crypt');
			$('input#link').val('www.justhvost.org/emoti.on/api.html?service='+sentences_crypt+'\\'+name_crypt+'&custom=true');
			got_love_here('2');
		} else
			alert("Error: No white space or empty strings\n\nTry again, please :)");
	}
}

function getParam(name) {
	name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	var regexS = "[\\?&]"+name+"=([^&#]*)";
	var regex = new RegExp(regexS);
	var results = regex.exec(window.location.href);
	if(results == null)
		return "";
	else
	return results[1];
}

function up(string) { return string[0].toUpperCase() + string.slice(1); }

function put_love(string){
	var split_sentence = string.split(".");
	for(var i=0; i < split_sentence.length; ++i){
		$('section').delay(900).append("</p>"+split_sentence[i]).animo({animation: 'bounceInLeft', duration: 0.5});
	}
}

function give_love() { 

	var sentences = new Array(
		"<p>I love my life because it gave me you. I love you because you are my life.",
		"<p>It's magic each time we hold each other, each time we cuddle, and each time we kiss. I feel goosebumps all over again. I never want to let you go for fear of losing you, so I just hold on a little bit tighter each day, refusing to let go. You will never know the warmth I feel inside me when I'm with you. You're all I ever wanted.",
		"<p>When I first saw you, you took my breath away. When you first talked to me, I couldn't think. When you asked me out, I couldn't respond. When you touched me, I got shivers all through my body. And when we first kissed, I floated away in my dreams.",
		"<p>I asked God for a minute and he gave me a day. I asked God for a flower and he gave me a bouquet. I asked God for love and he gave me that too. I asked God for an angel and he gave me you.",
		"<p>I love you more than words can define, feelings can express and thought can imagine.",
		"<p>Last night I looked up at the stars and matched each one with a reason why I love you. I was doing great, but then I ran out of stars.",
		"<p>Everyone says you only fall in love once but thats not true, everytime I hear your voice I fall in love all over again.",
		"<p>When you feel alone, just look at the spaces between your fingers, and remember that's where my fingers fit perfectly.",	
		"<p>If there was a card which said the right words, I would have bought it but there wasn't, that's why I'm writing this... I Love You.",
		"<p>Meeting you was fate. Becoming your friend was choice. But falling in love with you was completely out of my control.",
		"<p>You came suddenly and gave me two things; Patience to my heart and color to my life.",
		"<p>I miss you very much for every time when I think of you, my heart breaks into pieces and just a quick “Hello” from you brings the broken pieces back. Call me please.",
		"<p>No poems no fancy words, I just want the world to know that I love you my princess with all my heart.",
		"<p>Roads maybe sometimes rough but with you, no matter how rough the road is. I'll take no alternate route cause together we can make it through.. No matter how tough the going, I'll keep on going cause with you, my love the journey will never be boring."
	);

	var lol = getParam("service");
	var check = getParam("custom");
	if(check == "true") {
		var section = lol.split("\\");
		var sentence = names(section[0], "decrypt");
		var name = names(section[1], "decrypt");
		var sentence_up = up(sentence);
		var name_up = up(name);
		put_love(sentence);
		$("footer").append("<p id='from-to'>Written for: "+name_up).animo({animation: 'fadeIn', duration: 2});;
 	} else if(check == "false") {
		var section = lol.split("\\");
		var name1 = names(section[0], "decrypt");
		var name2 = names(section[1], "decrypt")
		var him = up(name1);
		var her = up(name2);
		var select = sentences[Math.floor(Math.random() * sentences.length)];
		put_love(select);
		$("footer").append("<p id='from-to'>From: "+him+"<br/>To: "+her+"</p>").animo({animation: 'fadeIn', duration: 2});
 	}
 }


 // Thanks to the stackoverflow.com community for some snippet. I`m not a coder. I`m a web designer.